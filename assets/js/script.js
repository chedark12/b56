// upon clicking the register button, it should execute the following validations:

//username should be greater than or equal to 10 chars
//password should be atleast 8 characters
//email should include the @ symbol
//use the Javascript includes() method to check if the email has an @ symbol
//password and pass2 should match
//all input boxes should not be empty
//create an errors variable that will check if there are any errors
//increment the errors variable every time we encounter an error in our input validations


	// declare an errors variable and assign 0 as its intial value
	// declare an username variable to store the value of the username input
	// declare a password variable to store the value of the password input
	// declare an email variable to store the value of the email input
	// declare an pass2 variable to store the value of the confirm password input
	


	// username should have atleast 10 characters



	//email should include the @ symbol
	// use the Javascript's includes function


	//password should be atleast 8 characters

	// confirm if the value of password is equal to pass2


	//if the value of errors is greater than 0, return false else 
	// create an alert message that says successfully registered! then
	//return true

let errors=0, 
	username, 
	password, 
	email, 
	pass2;


document.querySelector("#email").disabled = true;
document.querySelector("#password").disabled = true;
document.querySelector("#pass2").disabled = true;

ifUsername();
ifEmail();
ifPass();
ifPass2();

// REGISTER BUTTON
document.querySelector("#registerButton").addEventListener("click", ()=>{
	if (errors > 0) {
		Swal.fire({
			  position: 'top-end',
			  icon: 'error',
			  title: 'Passwords do not match',
			  showConfirmButton: true,
			  timer: 1500
			})
		//alert('passwords do not match')
		document.querySelector("#pass2").focus();
	}
	else{
		Swal.fire({
			  position: 'top-end',
			  icon: 'success',
			  title: 'Successfully registered',
			  showConfirmButton: true,
			  timer: 1500
			  
			})
			//syntax : localStorage.setItem("key", value)
			localStorage.setItem("username", document.querySelector("#username").value);
			window.location.href="./profile.html";
	}
})



function ifUsername(){
	username = document.querySelector("#username");
		
	username.addEventListener("keyup", ()=>{
		
		if (username.value.length > 9) {
			document.querySelector("#userErr").innerHTML = '<small class="alert-success">Username available</small>'//"Username available";
			document.querySelector("#email").disabled = false;
		}
		else{
			document.querySelector("#userErr").innerHTML = '<small class="alert-danger">Username should be atleast 10 characters</small>' //"Username should be atleast 10 characters";
			document.querySelector("#email").disabled = true;
		}
	})

}

function ifEmail(){
	email = document.querySelector("#email");
	email.addEventListener("keyup", () =>{
		if (email.value.includes('@') == true) {
			document.querySelector("#emailErr").innerHTML = '<small class="alert-success">email address valid</small>'
			document.querySelector("#password").disabled = false;

		}
		else{
			document.querySelector("#emailErr").innerHTML = '<small class="alert-danger">Enter a valid email</small>'
			document.querySelector("#email").focus()
			document.querySelector("#password").disabled = true;

		}
	})
}

function ifPass(){
	password = document.querySelector("#password");
		
	password.addEventListener("keyup", ()=>{
		
		if (password.value.length > 7) {
			document.querySelector("#passErr").innerHTML = '<small class="alert-success">That is a strong password</small>'
			document.querySelector("#pass2").disabled = false;
		}
		else{
			document.querySelector("#passErr").innerHTML = '<small class="alert-danger">Password should be atleast 8 characters</small>'
			document.querySelector("#pass2").disabled = true;
		}
	})	
}

function ifPass2(){
	pass2 = document.querySelector("#pass2");
		
	pass2.addEventListener("keyup", ()=>{
		
		if (pass2.value == password.value) {
			document.querySelector("#pass2Err").innerHTML = '<small class="alert-success">Password match</small>';
			errors = 0;
		}
		else{
			document.querySelector("#pass2Err").innerHTML = '<small class="alert-danger">Password do not match</small>';
			
			errors++;
		}
	})	
	return errors
}
	
	
